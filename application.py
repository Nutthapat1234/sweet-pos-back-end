import requests

from history import History
from product import Product
from os import path
from scb_payment import QRCodePayment
from datetime import datetime

# import RPi.GPIO as GPIO
# from mfrc522 import SimpleMFRC522


import time
import eventlet
import socketio

API_KEY = 'l7522f1258cab14922a970cccf93e1d8c7'
API_SECRET = 'cedd2bcfc7b2440b809ec6a3f91f997f'
MERCHANT = '849107492856441'
TERMINAL = '340169540836476'
BILLER_ID = '881174920492985'

sio = socketio.Server(cors_allowed_origins='*')
app = socketio.WSGIApp(sio, static_files={
    '/': {'content_type': 'text/html', 'filename': 'index.html'}
})
# reader = SimpleMFRC522()

connected = 0

products = {}
history = History()
list_id = []
list_quantity = []

qr = QRCodePayment(API_KEY, API_SECRET, MERCHANT, TERMINAL, BILLER_ID)


@sio.event
def connect(conn, addr):
    print("connected")
    global connected
    connected = 1


def read_key():
    global connected
    count = 0
    while True:
        # id, text = reader.read()
        if connected == 1:
            # addtoCart(uid, 1)
            sio.emit("message", getProductInfo('SK'))
            sio.sleep(10)
            count += 1
            if count == 4:
                connected = 0


def startApp():
    loadProducts()
    loadHistory()
    startSever()


def loadProducts():
    productFile = open("products.txt", "r")
    for line in productFile.readlines():
        info = line.split('|')
        products[str(info[0])] = Product(info[0], info[1], info[2], int(info[3].split('\n')[0]))
    productFile.close()


def loadHistory():
    if path.exists("history.txt"):
        historyFile = open("history.txt", "r")
        for line in historyFile.readlines():
            info = line.split('|')
            history.addItems(info[0], int(info[1].split("\n")[0]))
        historyFile.close()


def printProducts():
    num = 1
    for product in products.values():
        print(num, "=", product)
        num += 1


def getProduct():
    return products


def getProductInfo(uid):
    info = products[uid]
    return {
        'id': info.getId(),
        'title': info.getName(),
        'desc': info.getDescription(),
        'price': info.getPrice()
    }


def printHistoryInfo():
    print(history)
    for k, v in history.getItems().items():
        print("\t", products[k], "quantity :" + str(v))


def getHistoryInfo():
    return history


def addtoCart(uid, quantity):
    try:
        if uid in list_id:
            list_quantity[list_id.index(uid)] += quantity
        else:
            list_id.append(uid)
            list_quantity.append(quantity)
        return "addtoCart", {"status": 200, "message": "add to cart success"}
    except:
        return "addtoCart", {"status": 500, "message": "internal error"}


def removefromCart(uid, quantity):
    print("start removing...")
    if uid in list_id:
        list_quantity[list_id.index(uid)] -= quantity
        return "removefromCart", {"status": 200, "message": "remove success"}
    return "removefromCart", {"status": 204, "message": "remove fail"}


def checkout():
    print("start checkout...")
    all_total = 0
    row_total = []
    for index in range(len(list_id)):
        if list_quantity[index] != 0:
            total = list_quantity[index] * products[list_id[index]].getPrice()
            all_total += total
            row_total.append(total)
    qr = generateQR(all_total)
    return "checkout", {
        'total': all_total,
        'line_total': row_total,
        'qr': qr
    }


def confirm():
    print("start confirming....")
    time_start = datetime.now()
    success = 0
    while True:
        time_run = datetime.now() - time_start
        # res = int(requests.get("http://127.0.0.1:5000/getConfirm").json())
        res = 1
        if res == 1 or time_run.seconds > 60:
            if res == 1:
                success = 1
            break
        time.sleep(0.5)
    if success:
        for index in range(len(list_id)):
            history.increaseSale(list_id[index], list_quantity[index])
            history.toFile()
            reset()
        return "confirm", {"status": 200, "message": "confirm success, payment already transfer"}
    reset()
    return "confirm", {"status": 204, "message": "confirm fail, transaction not complete"}


def cancel():
    print("canceling....")
    reset()
    return "cancel", {"status": 200, "message": "cancel success"}


def reset():
    global list_id, list_quantity
    list_id = []
    list_quantity = []


@sio.event
def command(event, command):
    event_type, result = eval(command)
    sio.emit(event_type, result)


def startSever():
    print("Start Server...")
    eventlet.wsgi.server(eventlet.listen(('127.0.0.1', 8080)), app)


@sio.event
def startThread(ack):
    print("start thread...")
    sio.start_background_task(read_key)


def generateQR(amount):
    timestamp = int(time.time())
    qrString = qr.gererate_qr_30(
        amount=amount, ref1=f"SDS{timestamp}", ref2="", ref3="C01"
    )

    return qrString


def printCart():
    for e in list_id:
        print(list_id)
