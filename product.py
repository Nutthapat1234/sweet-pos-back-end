class Product:
    def __init__(self, id, name, desc, price):
        self.name = name
        self.id = id
        self.price = price
        self.desc = desc

    def getName(self):
        return self.name

    def setName(self, name):
        self.name = name

    def getId(self):
        return self.id

    def setId(self, id):
        self.id = id

    def getPrice(self):
        return self.price

    def setPrice(self, price):
        self.price = price

    def getDescription(self):
        return self.desc

    def setDesciption(self, desc):
        self.desc = desc

    def toFile(self):
        outProduct = open('products.txt', 'a')
        outProduct.write(str(self.id) + '|' + self.name + '|' + self.desc + '|' + str(self.price) + '\n')
        outProduct.close()

    def __str__(self):
        return "Id :" + self.id + " | Name :" + self.name + " | Price :" + str(self.price)
