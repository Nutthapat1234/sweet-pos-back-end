class History:
    def __init__(self):
        self.items = {}

    def addItems(self, id, quality):
        self.items[id] = quality

    def removeItem(self, itemId):
        for index in range(len(self.items)):
            if itemId == self.items[index].getId():
                del self.items[index]

    def getItems(self):
        return self.items

    def toFile(self):
        outInventory = open("history.txt", "w")
        for k, v in self.items.items():
            outInventory.write(str(k) + "|" + str(v) + "\n")
        outInventory.close()

    def __str__(self):
        return "History has " + str(len(self.items)) + " items"

    def increaseSale(self, id, q):
        q = int(q)
        if id in self.items.keys():
            self.items[id] += q
        else:
            self.items[id] = 1
