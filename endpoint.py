from flask import Flask, escape, request, jsonify

app = Flask(__name__)

confirmed = 0


@app.route('/confirm', methods=['POST'])
def confirm_payment():
    global confirmed
    data = request.data
    confirmed = 1
    return


@app.route('/getConfirm', methods=['GET'])
def get_confirm():
    global confirmed
    result = confirmed
    confirmed = 0
    return jsonify(result)


app.run()
