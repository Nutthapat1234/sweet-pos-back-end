import socketio

sio = socketio.Client()


@sio.event
def message(m):
    print(m)


sio.connect('http://127.0.0.1:8080')
# sio.emit("startThread")
sio.emit("command", "addtoCart('SK',1)")
sio.sleep(1)
sio.emit("command", "checkout()")
sio.sleep(1)
sio.emit("command", "confirm()")
